package br.edu.uniateneu.BolaoWS.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.BolaoWS.model.UsuarioEntity;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Long>{

@Query(value = "select u.cd_usuario, u.nm_login, u.ds_senha, u.nm_usuario " +
            " from tb_usuario u " +
            " where u.nm_login = (:login)", nativeQuery = true)
UsuarioEntity findUsuarioByLogin(@Param("login") String login);

@Query(value = "select u.cd_usuario, u.nm_login, u.ds_senha, u.nm_usuario " +
        " from tb_usuario u " +
        " where u.nm_login = (:login) and u.ds_senha = (:senha)", nativeQuery = true)
UsuarioEntity findUsuarioByLoginAndSenha(@Param("login") String login,@Param("senha") String senha);	
	
}