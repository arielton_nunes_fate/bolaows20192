package br.edu.uniateneu.BolaoWS.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.BolaoWS.model.GolEntity;

@Repository
public interface GolRepository extends JpaRepository<GolEntity, Long>{
	@Query(value = "select g.cd_gol,g.is_contra, g.qt_minutos, g.cd_partida " +
            " from tb_gol g " +
            " where g.cd_jogador = (:jogador)", nativeQuery = true)
List<GolEntity> findGolByJogador(@Param("jogador") Long jogador);

	@Query(value = "select g.cd_gol,g.is_contra, g.qt_minutos, g.cd_partida " +
            " from tb_gol g " +
            " where g.cd_partida = (:partida)", nativeQuery = true)
List<GolEntity> findGolByPartida(@Param("partida") Long partida);
	
	@Query(value = "select g.cd_gol,g.is_contra, g.qt_minutos, g.cd_partida " +
            " from tb_gol g " +
            " where g.is_contra = true", nativeQuery = true)
List<GolEntity> findGolsContra();

}