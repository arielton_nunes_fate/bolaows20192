package br.edu.uniateneu.BolaoWS.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.BolaoWS.model.JogadorEntity;
import br.edu.uniateneu.BolaoWS.model.TimeEntity;

@Repository
public interface TimeRepository extends JpaRepository<TimeEntity, Long>{
	@Query(value = "select t.cd_time, t.nm_escudo, t.nm_estadio, t.nm_estado, t.nm_time, t.nm_sigla " +
			" from tb_time t " +
			" where t.nm_sigla = (:sigla)", nativeQuery = true)
	public TimeEntity findTimeBySigla(@Param("sigla") String sigla);

	@Query(value = "select t.cd_time, t.nm_escudo, t.nm_estadio, t.nm_estado, t.nm_time, t.nm_sigla " +
			" from tb_time t " +
			" where t.nm_time = (:nome)", nativeQuery = true)
	public TimeEntity findTimeByNome(@Param("nome") String nome);

	@Query(value = "SELECT t.nm_time, j.nm_jogador, j.nm_apelido, j.cd_posicao, j.qt_idade, j.qt_peso, j.nr_camisa, j.nr_altura" +
			" FROM tb_jogador j INNER JOIN tb_time t on j.cd_time = t.cd_time" +
			" WHERE t.cd_time = :cdTime", nativeQuery = true)
	public List<JogadorEntity> findElencoByTime(@Param("cdTime") int cdTime);
}