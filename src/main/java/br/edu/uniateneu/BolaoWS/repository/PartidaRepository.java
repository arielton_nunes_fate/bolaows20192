package br.edu.uniateneu.BolaoWS.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.BolaoWS.model.PartidaEntity;

@Repository
public interface PartidaRepository extends JpaRepository<PartidaEntity, Long>{

	@Query(value = "select p.cd_partida, "
			+ "p.cd_mandante, "
			+ "p.pl_mandante, "
			+ "p.cd_visitante, "
			+ "p.pl_visitante, "
			+ "p.nm_local, "
			+ "p.dt_partida, "
			+ "p.cd_rodada "
			+ "from tb_partida p "
			+ "inner join tb_rodada r on p.cd_rodada=r.cd_rodada "
			+ "where r.nm_rodada = :numeroRodada "
			+ "and ((p.cd_mandante= (select cd_time from tb_time tm where tm.nm_time=:nomeTime))"
			+ "or (p.cd_visitante= (select cd_time from tb_time tm where tm.nm_time=:nomeTime)))" , nativeQuery = true)
	PartidaEntity findPartidaBYTimeRodada(@Param("nomeTime") String nomeTime,@Param("numeroRodada") int numeroRodada);	

	@Query(value = "select p.cd_partida, "
			+ "p.cd_mandante, "
			+ "p.pl_mandante, "
			+ "p.cd_visitante, "
			+ "p.pl_visitante, "
			+ "p.nm_local, "
			+ "p.dt_partida, "
			+ "p.cd_rodada "
			+ " from tb_partida p "
			+ "join tb_rodada r on p.cd_rodada=r.cd_rodada "
			+ "where r.nm_rodada = :numeroRodada", nativeQuery = true)
	List<PartidaEntity> findPartidasByRodada(@Param("numeroRodada")Integer numeroRodada);


}

