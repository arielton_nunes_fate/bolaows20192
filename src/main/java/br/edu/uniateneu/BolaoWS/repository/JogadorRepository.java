package br.edu.uniateneu.BolaoWS.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.BolaoWS.model.CartaoEntity;
import br.edu.uniateneu.BolaoWS.model.JogadorEntity;

@Repository
public interface JogadorRepository extends JpaRepository<JogadorEntity, Long>{
	@Query(value = "select c.cd_cartao, c.qt_minutos, c.tp_cartao, c.cd_jogador, c.cd_partida from "
			+ "tb_cartao c where c.cd_jogador =  = (:codigoJogador)", nativeQuery = true)
	public List<JogadorEntity> retornaJogadorPorNome(@Param("codigoJogador") long codigoJogador);
}