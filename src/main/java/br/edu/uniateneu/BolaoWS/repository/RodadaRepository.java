package br.edu.uniateneu.BolaoWS.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.BolaoWS.model.RodadaEntity;

@Repository
public interface RodadaRepository extends JpaRepository<RodadaEntity, Long>{

}