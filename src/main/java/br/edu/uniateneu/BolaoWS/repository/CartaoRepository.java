package br.edu.uniateneu.BolaoWS.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.BolaoWS.model.CartaoEntity;

@Repository
public interface CartaoRepository extends JpaRepository<CartaoEntity, Long> {
	@Query(value = "select c.cd_cartao, c.qt_minutos, c.tp_cartao, cd_partida " + " from tb_cartao c "
			+ " where c.cd_jogador = (:jogador)", nativeQuery = true)
	List<CartaoEntity> findCartaoByJogador(@Param("jogador") Long jogador);

	@Query(value = "select c.cd_cartao, c.qt_minutos, c.tp_cartao, cd_partida " + " from tb_cartao c "
			+ " where c.cd_partida = (:partida)", nativeQuery = true)
	List<CartaoEntity> findCartaoByPartida(@Param("partida") Long partida);

	@Query(value = "select c.cd_cartao, c.qt_minutos, c.tp_cartao, c.cd_jogador, c.cd_partida from "
			+ "tb_cartao c where c.cd_jogador =  = (:codigoJogador)", nativeQuery = true)
	
	
	
	public List<CartaoEntity> retornaCartaoPorJogador(@Param("codigoJogador") long codigoJogador);

	/*
	 * TODO desenvolver o método de listagem dos cartões pela partida.
	 * 
	 */

	@Query(value = "select c.cd_cartao, c.qt_minutos, c.tp_cartao, cd_partida " + " from tb_cartao c "
			+ " where c.tp_cartao = \"vermelho\"", nativeQuery = true)
	List<CartaoEntity> findCartoesVermelho();

}