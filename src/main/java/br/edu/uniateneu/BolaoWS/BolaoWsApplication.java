package br.edu.uniateneu.BolaoWS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@EntityScan("br.edu.uniateneu.BolaoWS.model*")
@EnableJpaRepositories("br.edu.uniateneu.BolaoWS.repository")
@SpringBootApplication
public class BolaoWsApplication { 

	public static void main(String[] args) {
		SpringApplication.run(BolaoWsApplication.class, args);
	}

}
