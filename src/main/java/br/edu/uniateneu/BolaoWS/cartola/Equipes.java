package br.edu.uniateneu.BolaoWS.cartola;

public class Equipes {
private Equipe mandante;
private Equipe visitante;
public Equipe getMandante() {
	return mandante;
}
public void setMandante(Equipe mandante) {
	this.mandante = mandante;
}
public Equipe getVisitante() {
	return visitante;
}
public void setVisitante(Equipe visitante) {
	this.visitante = visitante;
}
}
