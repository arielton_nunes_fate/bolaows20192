package br.edu.uniateneu.BolaoWS.cartola;

public class Scout {
	private Double A;
	private Double CA;
	private Double FC;
	private Double FF;
	private Double FS;
	private Double G;
	private Double PE;
	private Double RB;
	private Double SG;
	
	public Double getA() {
		return A;
	}
	public void setA(Double a) {
		A = a;
	}
	public Double getCA() {
		return CA;
	}
	public void setCA(Double cA) {
		CA = cA;
	}
	public Double getFC() {
		return FC;
	}
	public void setFC(Double fC) {
		FC = fC;
	}
	public Double getFF() {
		return FF;
	}
	public void setFF(Double fF) {
		FF = fF;
	}
	public Double getFS() {
		return FS;
	}
	public void setFS(Double fS) {
		FS = fS;
	}
	public Double getG() {
		return G;
	}
	public void setG(Double g) {
		G = g;
	}
	public Double getPE() {
		return PE;
	}
	public void setPE(Double pE) {
		PE = pE;
	}
	public Double getRB() {
		return RB;
	}
	public void setRB(Double rB) {
		RB = rB;
	}
	public Double getSG() {
		return SG;
	}
	public void setSG(Double sG) {
		SG = sG;
	}
}
