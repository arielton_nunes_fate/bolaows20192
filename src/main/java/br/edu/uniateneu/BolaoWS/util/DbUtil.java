package br.edu.uniateneu.BolaoWS.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.edu.uniateneu.BolaoWS.model.JogadorEntity;
import br.edu.uniateneu.BolaoWS.model.PartidaEntity;
import br.edu.uniateneu.BolaoWS.model.PosicaoEntity;
import br.edu.uniateneu.BolaoWS.model.RodadaEntity;
import br.edu.uniateneu.BolaoWS.model.TimeEntity;
import br.edu.uniateneu.BolaoWS.repository.JogadorRepository;
import br.edu.uniateneu.BolaoWS.repository.RodadaRepository;
import br.edu.uniateneu.BolaoWS.repository.TimeRepository;

public class DbUtil {
	@Autowired
	private static TimeRepository timeDAO;
	@Autowired
	private static JogadorRepository jogadorDAO;
	@Autowired
	private static RodadaRepository rodadaDAO;

	public static JogadorEntity novoJogador(String nome, PosicaoEntity posicao, int idade, Double peso,
			int numeroCamisa, double altura, TimeEntity time) {
		JogadorEntity jogador = new JogadorEntity();
		jogador.setNome(nome);
		jogador.setPosicao(posicao);
		jogador.setIdade(idade);
		jogador.setPeso(peso);
		jogador.setNumeroCamisa(numeroCamisa);
		jogador.setAltura(altura);
		jogador.setTime(time);
		return jogador;
	}

	public static List<RodadaEntity> criaRodadas() {
		List<RodadaEntity> rodadas = new ArrayList<RodadaEntity>();
		for (int j = 1; j < 39; j++) {
			RodadaEntity rodada = new RodadaEntity();
			rodada.setNumero(j);
			rodadas.add(rodada);
		}
		return rodadas;
	}

	public static PartidaEntity novaPartida(TimeEntity mandante, TimeEntity visitante, Date data) {
		PartidaEntity partidaEntity = new PartidaEntity();
		partidaEntity.setMandante(mandante);
		partidaEntity.setVisitante(visitante);
		partidaEntity.setData(data);
		partidaEntity.setLocal(mandante.getEstadio());
		return partidaEntity;
	}

	public static void preencheEntidadaes() {

		/** LISTA DE ELENCOS **/
		List<JogadorEntity> elencoCAP = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCAM = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoAVA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoBAH = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoBFG = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCEA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCHA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCOR = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCRU = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCSA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoFLA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoFLU = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoFOR = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoGOI = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoGRE = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoINT = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoPAL = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoSAN = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoSAO = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoVAS = new ArrayList<JogadorEntity>();

		/** CLUBES **/
		TimeEntity athleticoPR = new TimeEntity();
		athleticoPR.setNome("Athletico PR");
		athleticoPR.setEscudo("../../assets/icon/Athletico_Paranaense.svg");
		athleticoPR.setEstado("PR");
		athleticoPR.setEstadio("Arena da Baixada");
		athleticoPR.setSigla("CAP");
		athleticoPR.setElenco(elencoCAP);

		TimeEntity atleticoMG = new TimeEntity();
		atleticoMG.setNome("Atletico MG");
		atleticoMG.setEscudo("../../assets/icon/atletico-mg.svg");
		atleticoMG.setEstado("MG");
		atleticoMG.setEstadio("Mineirão");
		atleticoMG.setSigla("CAM");
		atleticoMG.setElenco(elencoCAM);

		TimeEntity avai = new TimeEntity();
		avai.setNome("Avai");
		avai.setEscudo("../../assets/icon/avai-futebol-clube.svg");
		avai.setEstado("SC");
		avai.setEstadio("Ressacada");
		avai.setSigla("AVA");
		avai.setElenco(elencoAVA);

		TimeEntity bahia = new TimeEntity();
		bahia.setNome("Bahia");
		bahia.setEscudo("../../assets/icon/bahia.svg");
		bahia.setEstado("BA");
		bahia.setEstadio("Fonte Nova");
		bahia.setSigla("BAH");
		bahia.setElenco(elencoBAH);

		TimeEntity botafogo = new TimeEntity();
		botafogo.setNome("Botafogo");
		botafogo.setEscudo("../../assets/icon/botafogo.svg");
		botafogo.setEstado("RJ");
		botafogo.setEstadio("Maracanã");
		botafogo.setSigla("BFG");
		botafogo.setElenco(elencoBFG);

		TimeEntity ceara = new TimeEntity();
		ceara.setNome("Ceara");
		ceara.setEscudo("../../assets/icon/ceara.svg");
		ceara.setEstado("CE");
		ceara.setEstadio("Castelão");
		ceara.setSigla("CEA");
		ceara.setElenco(elencoCEA);

		TimeEntity chapecoense = new TimeEntity();
		chapecoense.setNome("Chapecoense");
		chapecoense.setEscudo("../../assets/icon/chapecoense.svg");
		chapecoense.setEstado("SC");
		chapecoense.setEstadio("Arena Condá");
		chapecoense.setSigla("CHA");
		chapecoense.setElenco(elencoCHA);

		TimeEntity corinthians = new TimeEntity();
		corinthians.setNome("Corinthians");
		corinthians.setEscudo("../../assets/icon/corinthians.svg");
		corinthians.setEstado("SP");
		corinthians.setEstadio("Itaquerão");
		corinthians.setSigla("COR");
		corinthians.setElenco(elencoCOR);

		TimeEntity cruzeiro = new TimeEntity();
		cruzeiro.setNome("Cruzeiro");
		cruzeiro.setEscudo("../../assets/icon/cruzeiro.svg");
		cruzeiro.setEstado("MG");
		cruzeiro.setEstadio("Mineirão");
		cruzeiro.setSigla("CRU");
		cruzeiro.setElenco(elencoCRU);

		TimeEntity csa = new TimeEntity();
		csa.setNome("CSA");
		csa.setEscudo("../../assets/icon/csa.svg");
		csa.setEstado("AL");
		csa.setEstadio("Rei Pelé");
		csa.setSigla("CSA");
		csa.setElenco(elencoCSA);

		TimeEntity flamengo = new TimeEntity();
		flamengo.setNome("Flamengo");
		flamengo.setEscudo("../../assets/icon/Flamengo.svg");
		flamengo.setEstado("RJ");
		flamengo.setEstadio("Maracanã");
		flamengo.setSigla("FLA");
		flamengo.setElenco(elencoFLA);

		TimeEntity fluminense = new TimeEntity();
		fluminense.setNome("Fluminense");
		fluminense.setEscudo("../../assets/icon/fluminense.svg");
		fluminense.setEstado("RJ");
		fluminense.setEstadio("Maracanã");
		fluminense.setSigla("FLU");
		fluminense.setElenco(elencoFLU);

		TimeEntity fortaleza = new TimeEntity();
		fortaleza.setNome("Fortaleza");
		fortaleza.setEscudo("../../assets/icon/fortaleza.svg");
		fortaleza.setEstado("CE");
		fortaleza.setEstadio("Castelão");
		fortaleza.setSigla("FOR");
		fortaleza.setElenco(elencoFOR);

		TimeEntity goias = new TimeEntity();
		goias.setNome("Goiás");
		goias.setEscudo("../../assets/icon/goias.svg");
		goias.setEstado("GO");
		goias.setEstadio("Serra Dourada");
		goias.setSigla("GOI");
		goias.setElenco(elencoGOI);

		TimeEntity gremio = new TimeEntity();
		gremio.setNome("Grêmio");
		gremio.setEscudo("../../assets/icon/gremio.svg");
		gremio.setEstado("RS");
		gremio.setEstadio("Arena do Grêmio");
		gremio.setSigla("GRE");
		gremio.setElenco(elencoGRE);

		TimeEntity internacional = new TimeEntity();
		internacional.setNome("Internacional");
		internacional.setEscudo("../../assets/icon/internacional.svg");
		internacional.setEstado("RS");
		internacional.setEstadio("Beira Rio");
		internacional.setSigla("INT");
		internacional.setElenco(elencoINT);

		TimeEntity palmeiras = new TimeEntity();
		palmeiras.setNome("Palmeiras");
		palmeiras.setEscudo("../../assets/icon/palmeiras.svg");
		palmeiras.setEstado("SP");
		palmeiras.setEstadio("Allianz Parque");
		palmeiras.setSigla("PAL");
		palmeiras.setElenco(elencoPAL);

		TimeEntity santos = new TimeEntity();
		santos.setNome("Santos");
		santos.setEscudo("../../assets/icon/santos.svg");
		santos.setEstado("SP");
		santos.setEstadio("Vila Belmiro");
		santos.setSigla("SAN");
		santos.setElenco(elencoSAN);

		TimeEntity saoPaulo = new TimeEntity();
		saoPaulo.setNome("São Paulo");
		saoPaulo.setEscudo("../../assets/icon/sao-paulo.svg");
		saoPaulo.setEstado("SP");
		saoPaulo.setEstadio("Morumbi");
		saoPaulo.setSigla("SAO");
		saoPaulo.setElenco(elencoSAO);

		TimeEntity vasco = new TimeEntity();
		vasco.setNome("Vasco");
		vasco.setEscudo("../../assets/icon/vasco.svg");
		vasco.setEstado("RJ");
		vasco.setEstadio("São Januário");
		vasco.setSigla("VAS");
		vasco.setElenco(elencoVAS);

		timeDAO.save(athleticoPR);
		timeDAO.save(atleticoMG);
		timeDAO.save(avai);
		timeDAO.save(bahia);
		timeDAO.save(botafogo);
		timeDAO.save(ceara);
		timeDAO.save(chapecoense);
		timeDAO.save(corinthians);
		timeDAO.save(cruzeiro);
		timeDAO.save(csa);
		timeDAO.save(flamengo);
		timeDAO.save(fluminense);
		timeDAO.save(fortaleza);
		timeDAO.save(goias);
		timeDAO.save(gremio);
		timeDAO.save(internacional);
		timeDAO.save(palmeiras);
		timeDAO.save(santos);
		timeDAO.save(saoPaulo);
		timeDAO.save(vasco);

		/** JOGADORES **/
		/*
		 * JogadorEntity cAP1 = DbUtil.novoJogador("Santos", "Goleiro", 29, 0D, 1, 0D,
		 * athleticoPR); JogadorEntity cAP2 = DbUtil.novoJogador("Caio", "Goleiro", 22,
		 * 0D, 25, 0D, athleticoPR); JogadorEntity cAP3 = DbUtil.novoJogador("Léo",
		 * "Goleiro", 28, 0D, 29, 0D, athleticoPR); JogadorEntity cAP4 =
		 * DbUtil.novoJogador("Jonathan", "Lateral", 24, 0D, 2, 0D, athleticoPR);
		 * JogadorEntity cAP5 = DbUtil.novoJogador("M�rcio Azevedo", "Lateral", 29, 0D,
		 * 6, 0D, athleticoPR); JogadorEntity cAP6 = DbUtil.novoJogador("Renan Lodi",
		 * "lateral", 29, 0D, 12, 0D, athleticoPR); JogadorEntity cAP7 =
		 * DbUtil.novoJogador("Paulo Andr�", "Zagueiro", 29, 0D, 52, 0D, athleticoPR);
		 * JogadorEntity cAP8 = DbUtil.novoJogador("Lucas Halter", "Zagueiro", 29, 0D,
		 * 21, 0D, athleticoPR); JogadorEntity cAP9 = DbUtil.novoJogador("Madson",
		 * "Zagueiro", 29, 0D, 23, 0D, athleticoPR); JogadorEntity cAP10 =
		 * DbUtil.novoJogador("Thiago Heleno", "Zagueiro", 29, 0D, 44, 0D, athleticoPR);
		 * JogadorEntity cAP11 = DbUtil.novoJogador("Robson Bambu", "Zagueiro", 29, 0D,
		 * 14, 0D, athleticoPR); JogadorEntity cAP12 = DbUtil.novoJogador("Léo Pereira",
		 * "Zagueiro", 29, 0D, 4, 0D, athleticoPR); JogadorEntity cAP13 =
		 * DbUtil.novoJogador("Lucho Gonzalez", "Meia", 29, 0D, 3, 0D, athleticoPR);
		 * JogadorEntity cAP14 = DbUtil.novoJogador("Tomas Andrade", "Meia", 29, 0D, 8,
		 * 0D, athleticoPR); JogadorEntity cAP15 = DbUtil.novoJogador("Bruno Guimar�es",
		 * "Meia", 29, 0D, 39, 0D, athleticoPR); JogadorEntity cAP16 =
		 * DbUtil.novoJogador("Matheus Rossetto", "Meia", 29, 0D, 20, 0D, athleticoPR);
		 * JogadorEntity cAP17 = DbUtil.novoJogador("Thonny Anderson", "Meia", 29, 0D,
		 * 38, 0D, athleticoPR); JogadorEntity cAP18 = DbUtil.novoJogador("Welington",
		 * "Meia", 29, 0D, 5, 0D, athleticoPR); JogadorEntity cAP19 =
		 * DbUtil.novoJogador("Camacho", "Meia", 29, 0D, 15, 0D, athleticoPR);
		 * JogadorEntity cAP20 = DbUtil.novoJogador("L�o Cittadini", "Meia", 29, 0D, 18,
		 * 0D, athleticoPR); JogadorEntity cAP21 = DbUtil.novoJogador("Erick", "Meia",
		 * 29, 0D, 26, 0D, athleticoPR); JogadorEntity cAP22 =
		 * DbUtil.novoJogador("Bruno Naz�rio", "Meia", 29, 0D, 77, 0D, athleticoPR);
		 * JogadorEntity cAP23 = DbUtil.novoJogador("Rony", "Atacante", 24, 0D, 7, 0D,
		 * athleticoPR); JogadorEntity cAP24 = DbUtil.novoJogador("Marcelo Cirino",
		 * "Atacante", 27, 0D, 10, 0D, athleticoPR); JogadorEntity cAP25 =
		 * DbUtil.novoJogador("Braian Romero", "Atacante", 27, 0D, 17, 0D, athleticoPR);
		 * JogadorEntity cAP26 = DbUtil.novoJogador("Marco Rub�n", "Atacante", 32, 0D,
		 * 9, 0D, athleticoPR); JogadorEntity cAP27 = DbUtil.novoJogador("Nikão",
		 * "Atacante", 26, 0D, 11, 0D, athleticoPR); JogadorEntity cAP28=
		 * DbUtil.novoJogador("Vitinho", "Atacante", 20, 0D, 28, 0D, athleticoPR);
		 * 
		 * JogadorEntity cAM1 = DbUtil.novoJogador("Victor", "Goleiro", 29, 0D, 1, 0D,
		 * atleticoMG); JogadorEntity cAM2 = DbUtil.novoJogador("Michael", "Goleiro",
		 * 22, 0D, 31, 0D, atleticoMG); JogadorEntity cAM3 =
		 * DbUtil.novoJogador("Uilson", "Goleiro", 28, 0D, 32, 0D, atleticoMG);
		 * JogadorEntity cAM4 = DbUtil.novoJogador("Patric", "Lateral", 24, 0D, 2, 0D,
		 * atleticoMG); JogadorEntity cAM5 = DbUtil.novoJogador("Carlos Cesar",
		 * "Lateral", 26, 0D, 26, 0D, atleticoMG); JogadorEntity cAM6 =
		 * DbUtil.novoJogador("Fabio Santos", "Lateral", 29, 0D, 6, 0D, atleticoMG);
		 * JogadorEntity cAM7 = DbUtil.novoJogador("Leonardo Silva", "Zagueiro", 29, 0D,
		 * 3, 0D, atleticoMG); JogadorEntity cAM8 = DbUtil.novoJogador("Rever",
		 * "Zagueiro", 29, 0D, 4, 0D, atleticoMG); JogadorEntity cAM9 =
		 * DbUtil.novoJogador("Matheus Mancini", "Zagueiro", 29, 0D, 15, 0D,
		 * atleticoMG); JogadorEntity cAM10 = DbUtil.novoJogador("Igor Rabello",
		 * "Zagueiro", 29, 0D, 16, 0D, atleticoMG); JogadorEntity cAM11 =
		 * DbUtil.novoJogador("Iago Maidana", "Zagueiro", 29, 0D, 19, 0D, atleticoMG);
		 * JogadorEntity cAM12 = DbUtil.novoJogador("Martín Rea", "Zagueiro", 29, 0D,
		 * 38, 0D, atleticoMG); JogadorEntity cAM13 = DbUtil.novoJogador("Elias",
		 * "Meia", 29, 0D, 7, 0D, atleticoMG); JogadorEntity cAM14 =
		 * DbUtil.novoJogador("Zé Welison", "Meia", 29, 0D, 14, 0D, atleticoMG);
		 * JogadorEntity cAM15 = DbUtil.novoJogador("Cazares", "Meia", 29, 0D, 10, 0D,
		 * atleticoMG); JogadorEntity cAM16 = DbUtil.novoJogador("Alessandro", "Meia",
		 * 29, 0D, 39, 0D, atleticoMG); JogadorEntity cAM17 =
		 * DbUtil.novoJogador("Vin�cius", "Meia", 29, 0D, 92, 0D, atleticoMG);
		 * JogadorEntity cAM18 = DbUtil.novoJogador("Bruninho", "Meia", 29, 0D, 43, 0D,
		 * atleticoMG); JogadorEntity cAM19 = DbUtil.novoJogador("Luan", "Meia", 29, 0D,
		 * 27, 0D, atleticoMG); JogadorEntity cAM20 =
		 * DbUtil.novoJogador("Lucas Cândido", "Meia", 29, 0D, 18, 0D, atleticoMG);
		 * JogadorEntity cAM21 = DbUtil.novoJogador("Maicon", "Atacante", 29, 0D, 26,
		 * 0D, atleticoMG); JogadorEntity cAM22 = DbUtil.novoJogador("Yimmi Char�",
		 * "Atacante", 29, 0D, 8, 0D, atleticoMG); JogadorEntity cAM23 =
		 * DbUtil.novoJogador("Geuvânio", "Atacante", 24, 0D, 49, 0D, atleticoMG);
		 * JogadorEntity cAM24 = DbUtil.novoJogador("Alerrandro", "Atacante", 27, 0D,
		 * 44, 0D, atleticoMG); JogadorEntity cAM25 = DbUtil.novoJogador("Nathan",
		 * "Atacante", 27, 0D, 23, 0D, atleticoMG); JogadorEntity cAM26 =
		 * DbUtil.novoJogador("Ricardo Oliveira", "Atacante", 32, 0D, 9, 0D,
		 * atleticoMG); JogadorEntity cAM27 = DbUtil.novoJogador("Leandrinho",
		 * "Atacante", 26, 0D, 17, 0D, atleticoMG); JogadorEntity cAM28 =
		 * DbUtil.novoJogador("Papagaio", "Atacante", 20, 0D, 99, 0D, atleticoMG);
		 * JogadorEntity cAM29 = DbUtil.novoJogador("Lucas C�ndido", "Lateral", 29, 0D,
		 * 18, 0D, atleticoMG);
		 * 
		 * JogadorEntity aVA1 = DbUtil.novoJogador("Lucas Frigeri", "Goleiro", 29, 0D,
		 * 1, 0D, avai); JogadorEntity aVA2 = DbUtil.novoJogador("L�o Lopes", "Goleiro",
		 * 20, 0D, 13, 0D, avai); JogadorEntity aVA3 =
		 * DbUtil.novoJogador("Cl�udio Vitor", "Goleiro", 18, 0D, 22, 0D, avai);
		 * JogadorEntity aVA4 = DbUtil.novoJogador("Alex Silva", "Lateral", 24, 0D, 2,
		 * 0D, avai); JogadorEntity aVA5 = DbUtil.novoJogador("Igor Fernandes",
		 * "Lateral", 26, 0D, 6, 0D, avai); JogadorEntity aVA6 =
		 * DbUtil.novoJogador("Lucas Lovat", "Lateral", 21, 0D, 36, 0D, avai);
		 * JogadorEntity aVA7 = DbUtil.novoJogador("Leo", "Lateral", 27, 0D, 99, 0D,
		 * avai); JogadorEntity aVA8 = DbUtil.novoJogador("Bet�o", "Zagueiro", 34, 0D,
		 * 3, 0D, avai); JogadorEntity aVA9 = DbUtil.novoJogador("Fabian", "Zagueiro",
		 * 20, 0D, 14, 0D, avai); JogadorEntity aVA10 = DbUtil.novoJogador("Gustavo",
		 * "Zagueiro", 20, 0D, 15, 0D, avai); JogadorEntity aVA11 =
		 * DbUtil.novoJogador("Kunde", "Zagueiro", 20, 0D, 26, 0D, avai); JogadorEntity
		 * aVA12 = DbUtil.novoJogador("Marquinhos Silva", "Zagueiro", 35, 0D, 33, 0D,
		 * avai); JogadorEntity aVA13 = DbUtil.novoJogador("Maur�cio", "Zagueiro", 21,
		 * 0D, 6, 0D, avai); JogadorEntity aVA14 = DbUtil.novoJogador("Pedro Castro",
		 * "Meia", 29, 0D, 7, 0D, avai); JogadorEntity aVA15 =
		 * DbUtil.novoJogador("Menezes", "Meia", 21, 0D, 17, 0D, avai); JogadorEntity
		 * aVA16 = DbUtil.novoJogador("Andre Moritz", "Meia", 31, 0D, 30, 0D, avai);
		 * JogadorEntity aVA17 = DbUtil.novoJogador("Jones Carioca", "Meia", 30, 0D, 8,
		 * 0D, avai); JogadorEntity aVA18 = DbUtil.novoJogador("Douglas", "Meia", 36,
		 * 0D, 10, 0D, avai); JogadorEntity aVA19 = DbUtil.novoJogador("Geg�", "Meia",
		 * 24, 0D, 20, 0D, avai); JogadorEntity aVA20 =
		 * DbUtil.novoJogador("Luan Pereira", "Meia", 18, 0D, 21, 0D, avai);
		 * JogadorEntity aVA21 = DbUtil.novoJogador("Brenner", "Atacante", 25, 0D, 9,
		 * 0D, avai); JogadorEntity aVA22 = DbUtil.novoJogador("Get�lio", "Atacante",
		 * 21, 0D, 11, 0D, avai); JogadorEntity aVA23 =
		 * DbUtil.novoJogador("Daniel Amorim", "Atacante", 29, 0D, 28, 0D, avai);
		 * JogadorEntity aVA24 = DbUtil.novoJogador("Matheus Matias", "Atacante", 20,
		 * 0D, 29, 0D, avai); JogadorEntity aVA25 = DbUtil.novoJogador("Alisson",
		 * "Atacante", 21, 0D, 35, 0D, avai);
		 * 
		 * 
		 * elencoCAP.add(cAP1); elencoCAP.add(cAP2); elencoCAP.add(cAP3);
		 * elencoCAP.add(cAP4); elencoCAP.add(cAP5); elencoCAP.add(cAP6);
		 * elencoCAP.add(cAP7); elencoCAP.add(cAP8); elencoCAP.add(cAP9);
		 * elencoCAP.add(cAP10); elencoCAP.add(cAP11); elencoCAP.add(cAP12);
		 * elencoCAP.add(cAP13); elencoCAP.add(cAP14); elencoCAP.add(cAP15);
		 * elencoCAP.add(cAP16); elencoCAP.add(cAP17); elencoCAP.add(cAP18);
		 * elencoCAP.add(cAP19); elencoCAP.add(cAP20); elencoCAP.add(cAP21);
		 * elencoCAP.add(cAP22); elencoCAP.add(cAP23); elencoCAP.add(cAP24);
		 * elencoCAP.add(cAP25); elencoCAP.add(cAP26); elencoCAP.add(cAP27);
		 * elencoCAP.add(cAP28); athleticoPR.setElenco(elencoCAP);
		 * 
		 * elencoCAM.add(cAM1); elencoCAM.add(cAM2); elencoCAM.add(cAM3);
		 * elencoCAM.add(cAM4); elencoCAM.add(cAM5); elencoCAM.add(cAM6);
		 * elencoCAM.add(cAM7); elencoCAM.add(cAM8); elencoCAM.add(cAM9);
		 * elencoCAM.add(cAM10); elencoCAM.add(cAM11); elencoCAM.add(cAM12);
		 * elencoCAM.add(cAM13); elencoCAM.add(cAM14); elencoCAM.add(cAM15);
		 * elencoCAM.add(cAM16); elencoCAM.add(cAM17); elencoCAM.add(cAM18);
		 * elencoCAM.add(cAM19); elencoCAM.add(cAM20); elencoCAM.add(cAM21);
		 * elencoCAM.add(cAM22); elencoCAM.add(cAM23); elencoCAM.add(cAM24);
		 * elencoCAM.add(cAM25); elencoCAM.add(cAM26); elencoCAM.add(cAM27);
		 * elencoCAM.add(cAM28); elencoCAM.add(cAM29); atleticoMG.setElenco(elencoCAM);
		 * 
		 * elencoAVA.add(aVA1); elencoAVA.add(aVA2); elencoAVA.add(aVA3);
		 * elencoAVA.add(aVA4); elencoAVA.add(aVA5); elencoAVA.add(aVA6);
		 * elencoAVA.add(aVA7); elencoAVA.add(aVA8); elencoAVA.add(aVA9);
		 * elencoAVA.add(aVA10); elencoAVA.add(aVA11); elencoAVA.add(aVA12);
		 * elencoAVA.add(aVA13); elencoAVA.add(aVA14); elencoAVA.add(aVA15);
		 * elencoAVA.add(aVA16); elencoAVA.add(aVA17); elencoAVA.add(aVA18);
		 * elencoAVA.add(aVA19); elencoAVA.add(aVA20); elencoAVA.add(aVA21);
		 * elencoAVA.add(aVA22); elencoAVA.add(aVA23); elencoAVA.add(aVA24);
		 * elencoAVA.add(aVA25); avai.setElenco(elencoAVA); /* /**PERSISTIR JOGADORES
		 **/
		/*
		 * jogadorDAO.save(cAP1); jogadorDAO.save(cAP2); jogadorDAO.save(cAP3);
		 * jogadorDAO.save(cAP4); jogadorDAO.save(cAP5); jogadorDAO.save(cAP6);
		 * jogadorDAO.save(cAP7); jogadorDAO.save(cAP8); jogadorDAO.save(cAP9);
		 * jogadorDAO.save(cAP10); jogadorDAO.save(cAP11); jogadorDAO.save(cAP12);
		 * jogadorDAO.save(cAP13); jogadorDAO.save(cAP14); jogadorDAO.save(cAP15);
		 * jogadorDAO.save(cAP16); jogadorDAO.save(cAP17); jogadorDAO.save(cAP18);
		 * jogadorDAO.save(cAP19); jogadorDAO.save(cAP20); jogadorDAO.save(cAP21);
		 * jogadorDAO.save(cAP22); jogadorDAO.save(cAP23); jogadorDAO.save(cAP24);
		 * jogadorDAO.save(cAP25); jogadorDAO.save(cAP26); jogadorDAO.save(cAP27);
		 * jogadorDAO.save(cAP28);
		 * 
		 * jogadorDAO.save(cAM1); jogadorDAO.save(cAM2); jogadorDAO.save(cAM3);
		 * jogadorDAO.save(cAM4); jogadorDAO.save(cAM5); jogadorDAO.save(cAM6);
		 * jogadorDAO.save(cAM7); jogadorDAO.save(cAM8); jogadorDAO.save(cAM9);
		 * jogadorDAO.save(cAM10); jogadorDAO.save(cAM11); jogadorDAO.save(cAM12);
		 * jogadorDAO.save(cAM13); jogadorDAO.save(cAM14); jogadorDAO.save(cAM15);
		 * jogadorDAO.save(cAM16); jogadorDAO.save(cAM17); jogadorDAO.save(cAM18);
		 * jogadorDAO.save(cAM19); jogadorDAO.save(cAM20); jogadorDAO.save(cAM21);
		 * jogadorDAO.save(cAM22); jogadorDAO.save(cAM23); jogadorDAO.save(cAM24);
		 * jogadorDAO.save(cAM25); jogadorDAO.save(cAM26); jogadorDAO.save(cAM27);
		 * jogadorDAO.save(cAM28); jogadorDAO.save(cAM29);
		 * 
		 * jogadorDAO.save(aVA1); jogadorDAO.save(aVA2); jogadorDAO.save(aVA3);
		 * jogadorDAO.save(aVA4); jogadorDAO.save(aVA5); jogadorDAO.save(aVA6);
		 * jogadorDAO.save(aVA7); jogadorDAO.save(aVA8); jogadorDAO.save(aVA9);
		 * jogadorDAO.save(aVA10); jogadorDAO.save(aVA11); jogadorDAO.save(aVA12);
		 * jogadorDAO.save(aVA13); jogadorDAO.save(aVA14); jogadorDAO.save(aVA15);
		 * jogadorDAO.save(aVA16); jogadorDAO.save(aVA17); jogadorDAO.save(aVA18);
		 * jogadorDAO.save(aVA19); jogadorDAO.save(aVA20); jogadorDAO.save(aVA21);
		 * jogadorDAO.save(aVA22); jogadorDAO.save(aVA23); jogadorDAO.save(aVA24);
		 * jogadorDAO.save(aVA25);
		 */
		List<RodadaEntity> rodadas = DbUtil.criaRodadas();
		for (RodadaEntity rodadaEntity : rodadas) {
			rodadaDAO.save(rodadaEntity);
		}

		/*
		 * 
		 * new TimeEntity("Athletico PR","PR",
		 * "assets/icon/Athletico_Paranaense.svg","Arena da Baixada","CAP"); TimeEntity
		 * atleticoMG = new
		 * TimeEntity("Atl�tico MG","MG","../../assets/icon/atletico-mg.svg","Mineir�o",
		 * "CAM"); TimeEntity avai = new
		 * TimeEntity("Ava�","SC","../../assets/icon/avai-futebol-clube.svg","Ressacada"
		 * ,"AVA"); TimeEntity bahia = new
		 * TimeEntity("Bahia","BA","../../assets/icon/bahia.svg","Fonte Nova","BAH");
		 * TimeEntity botafogo = new
		 * TimeEntity("Botafogo","RJ","../../assets/icon/botafogo.svg","Nilton Santos"
		 * ,"BFG"); TimeEntity ceara = new
		 * TimeEntity("Cear�","CE","../../assets/icon/ceara.svg","Castel�o","CEA");
		 * TimeEntity chapecoense = new TimeEntity("Chapecoense","SC",
		 * "../../assets/icon/chapecoense.svg","Arena Cond�","CHA"); TimeEntity
		 * corinthians = new
		 * TimeEntity("Corinthians","SP","../../assets/icon/corinthians.svg","Itaquer�o"
		 * ,"COR"); TimeEntity cruzeiro = new
		 * TimeEnti.ty("Cruzeiro","RJ","../../assets/icon/cruzeiro.svg","Mineir�o","CRU"
		 * ); TimeEntity csa = new
		 * TimeEntity("CSA","AL","../../assets/icon/csa.svg","Rei Pel�","CSA");
		 * TimeEntity flamengo = new
		 * TimeEntity("Flamengo","RJ","../../assets/icon/Flamengo.svg","Maracan�","FLA")
		 * ; TimeEntity fluminense = new
		 * TimeEntity("Fluminense","RJ","../../assets/icon/fluminense.svg","Maracan�",
		 * "FLU"); TimeEntity fortaleza = new
		 * TimeEntity("Fortaleza","CE","../../assets/icon/fortaleza.svg","Castel�o",
		 * "FOR"); TimeEntity goias = new
		 * TimeEntity("Goi�s","GO","../../assets/icon/goias.svg","Serra Dourada","GOI");
		 * TimeEntity gremio = new
		 * TimeEntity("Gr�mio","RS","../../assets/icon/gremio.svg","Arena do Gr�mio"
		 * ,"GRE"); TimeEntity internacional = new TimeEntity("Internacional","RS",
		 * "../../assets/icon/internacional.svg","Beira Rio","INT"); TimeEntity
		 * palmeiras = new TimeEntity("Palmeiras","SP",
		 * "../../assets/icon/palmeiras.svg","Allianz Parque","PAL"); TimeEntity santos
		 * = new
		 * TimeEntity("Santos","SP","../../assets/icon/santos.svg","Vila Belmiro","SAN")
		 * ; TimeEntity saoPaulo = new
		 * TimeEntity("S�o Paulo","SP","../../assets/icon/sao-paulo.svg","Morumbi","SAO"
		 * ); TimeEntity vasco = new
		 * TimeEntity("Vasco da Gama","RJ","../../assets/icon/vasco.svg","S�o Janu�rio"
		 * ,"VAS");
		 * 
		 */

	}

	public static void main(String[] args) {

		DbUtil.preencheEntidadaes();
	}
}
