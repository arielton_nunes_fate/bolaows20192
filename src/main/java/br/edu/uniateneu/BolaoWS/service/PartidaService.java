package br.edu.uniateneu.BolaoWS.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.BolaoWS.model.PartidaEntity;
import br.edu.uniateneu.BolaoWS.model.ResponseModel;
import br.edu.uniateneu.BolaoWS.repository.PartidaRepository;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/service")
public class PartidaService {

	@Autowired
	private PartidaRepository partidaRepository;

	/**
	 * SALVAR UM NOVO REGISTRO
	 * 
	 * @param partida
	 * @return
	 */
	@RequestMapping(value = "/partida", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel salvar(@RequestBody PartidaEntity partida) {

		try {

			this.partidaRepository.save(partida);

			return new ResponseModel(1, "Registro salvo com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * ATUALIZAR O REGISTRO DE UMA PESSOA
	 * 
	 * @param partida
	 * @return
	 */
	@RequestMapping(value = "/partida", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel atualizar(@RequestBody PartidaEntity partida) {

		try {

			this.partidaRepository.save(partida);

			return new ResponseModel(1, "Registro atualizado com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * CONSULTAR TODAS AS PESSOAS
	 * 
	 * @return
	 */
	@RequestMapping(value = "/partida", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<PartidaEntity> consultar() {

		return this.partidaRepository.findAll();
	}

	/**
	 * BUSCAR UMA PESSOA PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/partida/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody PartidaEntity buscar(@PathVariable("codigo") Long codigo) {

		return this.partidaRepository.getOne(codigo);
	}

	/**
	 * BUSCAR PARTIDAS PELO NUMERO DA RODADA
	 * 
	 * @param numeroRodada
	 * @return
	 */
	@RequestMapping(value = "/partida/rodada/{numeroRodada}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<PartidaEntity> buscar(@PathVariable("numeroRodada") Integer numeroRodada) {

		return this.partidaRepository.findPartidasByRodada(numeroRodada);
	}

	/***
	 * EXCLUIR UM REGISTRO PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */

	@RequestMapping(value = "/partida/rodada/{numRodada}/{nomeTime}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody PartidaEntity getPartidaByTimeRodada(@PathVariable("numRodada") int numRodada,
			@PathVariable("nomeTime") String nomeTime) {

		// return this.partidaRepository.findPartidaBYTimeRodada(nomeTime, numRodada);
		List<PartidaEntity> partidas = this.partidaRepository.findPartidasByRodada(numRodada);
		for (PartidaEntity partidaEntity : partidas) {
			if (partidaEntity.getMandante().getNome().equalsIgnoreCase(nomeTime)
					|| partidaEntity.getVisitante().getNome().equalsIgnoreCase(nomeTime)) {
				return partidaEntity;
			}
		}
		return null;

	}

}
