package br.edu.uniateneu.BolaoWS.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.BolaoWS.model.CartaoEntity;
import br.edu.uniateneu.BolaoWS.model.ResponseModel;
import br.edu.uniateneu.BolaoWS.repository.CartaoRepository;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/service")
public class CartaoService {

	@Autowired
	private CartaoRepository cartaoRepository;

	/**
	 * SALVAR UM NOVO REGISTRO
	 * 
	 * @param cartao
	 * @return
	 */
	@RequestMapping(value = "/cartao", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel salvar(@RequestBody CartaoEntity cartao) {

		try {

			this.cartaoRepository.save(cartao);

			return new ResponseModel(1, "Registro salvo com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * ATUALIZAR O REGISTRO DE UMA PESSOA
	 * 
	 * @param cartao
	 * @return
	 */
	@RequestMapping(value = "/cartao", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel atualizar(@RequestBody CartaoEntity cartao) {

		try {

			this.cartaoRepository.save(cartao);

			return new ResponseModel(1, "Registro atualizado com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * CONSULTAR TODAS AS PESSOAS
	 * 
	 * @return
	 */
	@RequestMapping(value = "/cartao", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<CartaoEntity> consultar() {

		return this.cartaoRepository.findAll();
	}

	/**
	 * BUSCAR UMA PESSOA PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/cartao/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody CartaoEntity buscar(@PathVariable("codigo") Long codigo) {

		return this.cartaoRepository.getOne(codigo);
	}

	/***
	 * EXCLUIR UM REGISTRO PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/cartao/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {

		CartaoEntity cartaoEntity = cartaoRepository.getOne(codigo);

		try {

			cartaoRepository.delete(cartaoEntity);

			return new ResponseModel(1, "Registro excluido com sucesso!");

		} catch (Exception e) {
			return new ResponseModel(0, e.getMessage());
		}
	}

}
