package br.edu.uniateneu.BolaoWS.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.edu.uniateneu.BolaoWS.cartola.Partida;
import br.edu.uniateneu.BolaoWS.cartola.Rodada;
import br.edu.uniateneu.BolaoWS.model.PartidaEntity;
import br.edu.uniateneu.BolaoWS.model.ResponseModel;
import br.edu.uniateneu.BolaoWS.model.RodadaEntity;
import br.edu.uniateneu.BolaoWS.repository.PartidaRepository;
import br.edu.uniateneu.BolaoWS.repository.RodadaRepository;
import br.edu.uniateneu.BolaoWS.repository.TimeRepository;
import br.edu.uniateneu.BolaoWS.util.StringUtil;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/service")
public class RodadaService {

	@Autowired
	private RodadaRepository rodadaRepository;
	
	@Autowired
	private PartidaRepository partidaRepository;
	
	@Autowired
	private TimeRepository timeRepository;
	
	/**
	 * SALVAR UM NOVO REGISTRO
	 * 
	 * @param rodada
	 * @return
	 */
	@RequestMapping(value = "/rodada", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel salvar(@RequestBody RodadaEntity rodada) {

		try {

			this.rodadaRepository.save(rodada);

			return new ResponseModel(1, "Registro salvo com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * ATUALIZAR O REGISTRO DE UMA PESSOA
	 * 
	 * @param rodada
	 * @return
	 */
	@RequestMapping(value = "/rodada", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel atualizar(@RequestBody RodadaEntity rodada) {

		try {

			this.rodadaRepository.save(rodada);

			return new ResponseModel(1, "Registro atualizado com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * CONSULTAR TODAS AS PESSOAS
	 * 
	 * @return
	 */
	@RequestMapping(value = "/rodada", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<RodadaEntity> consultar() {

		return this.rodadaRepository.findAll();
	}

	/**
	 * BUSCAR UMA PESSOA PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/rodada/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody RodadaEntity buscar(@PathVariable("codigo") Long codigo) {

		return this.rodadaRepository.getOne(codigo);
	}

	/***
	 * EXCLUIR UM REGISTRO PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/rodada/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {

		RodadaEntity rodadaEntity = rodadaRepository.getOne(codigo);

		try {

			rodadaRepository.delete(rodadaEntity);

			return new ResponseModel(1, "Registro excluido com sucesso!");

		} catch (Exception e) {
			return new ResponseModel(0, e.getMessage());
		}
	}


	@RequestMapping(value = "/rodada/preenche", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel preencher() {
		String partidas="";
		Gson gson = new Gson();
		try {
			for (Integer numRodada = 1; numRodada < 39; numRodada++) {
		
				RodadaEntity rodadaEntity = new RodadaEntity();
				rodadaEntity.setNumero(numRodada);
				rodadaRepository.save(rodadaEntity);
				ArrayList<PartidaEntity> partidaEntities = new ArrayList<PartidaEntity>();
				File f = new File(
						"C:\\Ateneu\\2019.2\\Webservices\\workspace\\git\\BolaoWS\\src\\main\\resources\\rodadas\\"+ numRodada +".json");
				BufferedReader br = new BufferedReader(new FileReader(f));
				String jsonTxt = "";
				String linha = "";
				while ((linha = br.readLine()) != null) {
					jsonTxt += linha;
				}
				br.close();
				jsonTxt = StringUtil.decodificar(jsonTxt);
				List<Partida> partidasObj;
				Type usuariosListType = new TypeToken<ArrayList<Partida>>(){}.getType(); 
				partidasObj = gson.fromJson(jsonTxt, usuariosListType);
				for (Partida partida : partidasObj) {
					PartidaEntity entidade = partida.convertePartidaParaEntidade();
					entidade.setMandante(timeRepository.findTimeBySigla(partida.getEquipes().getMandante().getSigla()));;
					entidade.setVisitante(timeRepository.findTimeBySigla(partida.getEquipes().getVisitante().getSigla()));;
					partidas += "Adicionada a partida " + 
					partida.getEquipes().getMandante().getNome_popular() + " x " + 
					partida.getEquipes().getVisitante().getNome_popular() +"; \n";
					entidade.setRodada(rodadaEntity);
					partidaRepository.save(entidade);
				}
				//rodadaEntity.setPartidas(partidaEntities);	
			}
		} catch (Exception e) {
			return new ResponseModel(0, e.getMessage());
		}
		return new ResponseModel(1, partidas);
	}

}
