package br.edu.uniateneu.BolaoWS.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import br.edu.uniateneu.BolaoWS.cartola.Atletas;
import br.edu.uniateneu.BolaoWS.cartola.Jogador;
import br.edu.uniateneu.BolaoWS.model.JogadorEntity;
import br.edu.uniateneu.BolaoWS.model.ResponseModel;
import br.edu.uniateneu.BolaoWS.model.TimeEntity;
import br.edu.uniateneu.BolaoWS.repository.JogadorRepository;
import br.edu.uniateneu.BolaoWS.repository.TimeRepository;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/service")
public class JogadorService {

	@Autowired
	private JogadorRepository jogadorRepository;
	@Autowired
	private TimeRepository timeRepository;

	/**
	 * SALVAR UM NOVO REGISTRO
	 * 
	 * @param jogador
	 * @return
	 */
	@RequestMapping(value = "/jogador", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel salvar(@RequestBody JogadorEntity jogador) {

		try {

			this.jogadorRepository.save(jogador);

			return new ResponseModel(1, "Registro salvo com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * ATUALIZAR O REGISTRO DE UMA PESSOA
	 * 
	 * @param jogador
	 * @return
	 */
	@RequestMapping(value = "/jogador", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel atualizar(@RequestBody JogadorEntity jogador) {

		try {

			this.jogadorRepository.save(jogador);

			return new ResponseModel(1, "Registro atualizado com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * CONSULTAR TODAS AS PESSOAS
	 * 
	 * @return
	 */
	@RequestMapping(value = "/jogador", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<JogadorEntity> consultar() {

		return this.jogadorRepository.findAll();
	}

	/**
	 * BUSCAR UMA PESSOA PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/jogador/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody JogadorEntity buscar(@PathVariable("codigo") Long codigo) {

		return this.jogadorRepository.getOne(codigo);
	}

	/***
	 * EXCLUIR UM REGISTRO PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/jogador/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {

		JogadorEntity jogadorEntity = jogadorRepository.getOne(codigo);

		try {

			jogadorRepository.delete(jogadorEntity);

			return new ResponseModel(1, "Registro excluido com sucesso!");

		} catch (Exception e) {
			return new ResponseModel(0, e.getMessage());
		}
	}

	@RequestMapping(value = "/jogador/preenche", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel preencher() {
		String jogadores="";
		Gson gson = new Gson();
		try {

			File f = new File(
					"C:\\Ateneu\\2019.2\\Webservices\\workspace\\git\\BolaoWS\\src\\main\\java\\br\\edu\\uniateneu\\BolaoWS\\cartola\\jogador.json");
			BufferedReader br = new BufferedReader(new FileReader(f));
			String jsonTxt = "";
			String linha = "";
			while ((linha = br.readLine()) != null) {
				jsonTxt += linha;
			}
			br.close();

			Atletas atletas;
			atletas = gson.fromJson(jsonTxt, Atletas.class);
			for (Object object : atletas.getAtletas()) {
				Jogador jogador = (Jogador) object;
				JogadorEntity entidade = jogador.converteJogadorParaEntidade();
				entidade.setTime(getTimeEntityDoJSON(jogador.getClube_id()));
				this.jogadorRepository.save(entidade);
				jogadores += jogador.getApelido()+" Salvo com sucesso\n";
			}
		} catch (Exception e) {
			return new ResponseModel(0, e.getMessage());
		}
		return new ResponseModel(1, jogadores);
	}

	public TimeEntity getTimeEntityDoJSON(int codigo) {

		String nome = null;
		switch (codigo) {
		case 293:
			nome ="Athletico PR";
			break;
		case 282:
			nome ="Atletico MG";
			break;
		case 314:
			nome ="Avai";
			break;
		case 265:
			nome ="Bahia";
			break;
		case 263:
			nome ="Botafogo";
			break;
		case 354:
			nome ="Ceará";
			break;
		case 315:
			nome ="Chapecoense";
			break;
		case 264:
			nome ="Corinthians";
			break;
		case 283:
			nome ="Cruzeiro";
			break;
		case 341:
			nome ="CSA";
			break;
		case 262:
			nome ="Flamengo";
			break;
		case 266:
			nome ="Fluminense";
			break;
		case 356:
			nome ="Fortaleza";
			break;
		case 290:
			nome ="Goiás";
			break;
		case 284:
			nome ="Grêmio";
			break;
		case 285:
			nome ="Internacional";
			break;
		case 275:
			nome ="Palmeiras";
			break;
		case 277:
			nome ="Santos";
			break;
		case 276:
			nome ="São Paulo";
			break;
		case 267:
			nome ="Vasco";
			break;
		default:
			break;
		}
			return this.timeRepository.findTimeByNome(nome);
		}
	}
