package br.edu.uniateneu.BolaoWS.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.BolaoWS.model.TimeEntity;
import br.edu.uniateneu.BolaoWS.model.JogadorEntity;
import br.edu.uniateneu.BolaoWS.model.PartidaEntity;
import br.edu.uniateneu.BolaoWS.model.ResponseModel;
import br.edu.uniateneu.BolaoWS.repository.TimeRepository;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/service")
public class TimeService {

	@Autowired
	private TimeRepository timeRepository;

	/**
	 * SALVAR UM NOVO REGISTRO
	 * 
	 * @param time
	 * @return
	 */
	@RequestMapping(value = "/time", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel salvar(@RequestBody TimeEntity time) {

		try {

			this.timeRepository.save(time);

			return new ResponseModel(1, "Registro salvo com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * ATUALIZAR O REGISTRO DE UMA PESSOA
	 * 
	 * @param time
	 * @return
	 */
	@RequestMapping(value = "/time", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel atualizar(@RequestBody TimeEntity time) {

		try {

			this.timeRepository.save(time);

			return new ResponseModel(1, "Registro atualizado com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	
	
	
	@RequestMapping(value = "/time/preenche", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody void preencher() {

		List<JogadorEntity> elencoCAP = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCAM = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoAVA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoBAH = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoBFG = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCEA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCHA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCOR = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCRU = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoCSA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoFLA = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoFLU = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoFOR = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoGOI = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoGRE = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoINT = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoPAL = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoSAN = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoSAO = new ArrayList<JogadorEntity>();
		List<JogadorEntity> elencoVAS = new ArrayList<JogadorEntity>();

		/** CLUBES **/
		TimeEntity athleticoPR = new TimeEntity();
		athleticoPR.setNome("Athletico PR");
		athleticoPR.setEscudo("../../assets/icon/Athletico_Paranaense.svg");
		athleticoPR.setEstado("PR");
		athleticoPR.setEstadio("Arena da Baixada");
		athleticoPR.setSigla("CAP");
		athleticoPR.setElenco(elencoCAP);

		TimeEntity atleticoMG = new TimeEntity();
		atleticoMG.setNome("Atletico MG");
		atleticoMG.setEscudo("../../assets/icon/atletico-mg.svg");
		atleticoMG.setEstado("MG");
		atleticoMG.setEstadio("Mineirão");
		atleticoMG.setSigla("CAM");
		atleticoMG.setElenco(elencoCAM);

		TimeEntity avai = new TimeEntity();
		avai.setNome("Avai");
		avai.setEscudo("../../assets/icon/avai-futebol-clube.svg");
		avai.setEstado("SC");
		avai.setEstadio("Ressacada");
		avai.setSigla("AVA");
		avai.setElenco(elencoAVA);

		TimeEntity bahia = new TimeEntity();
		bahia.setNome("Bahia");
		bahia.setEscudo("../../assets/icon/bahia.svg");
		bahia.setEstado("BA");
		bahia.setEstadio("Fonte Nova");
		bahia.setSigla("BAH");
		bahia.setElenco(elencoBAH);

		TimeEntity botafogo = new TimeEntity();
		botafogo.setNome("Botafogo");
		botafogo.setEscudo("../../assets/icon/botafogo.svg");
		botafogo.setEstado("RJ");
		botafogo.setEstadio("Maracanã");
		botafogo.setSigla("BOT");
		botafogo.setElenco(elencoBFG);

		TimeEntity ceara = new TimeEntity();
		ceara.setNome("Ceara");
		ceara.setEscudo("../../assets/icon/ceara.svg");
		ceara.setEstado("CE");
		ceara.setEstadio("Castelão");
		ceara.setSigla("CEA");
		ceara.setElenco(elencoCEA);

		TimeEntity chapecoense = new TimeEntity();
		chapecoense.setNome("Chapecoense");
		chapecoense.setEscudo("../../assets/icon/chapecoense.svg");
		chapecoense.setEstado("SC");
		chapecoense.setEstadio("Arena Condá");
		chapecoense.setSigla("CHA");
		chapecoense.setElenco(elencoCHA);

		TimeEntity corinthians = new TimeEntity();
		corinthians.setNome("Corinthians");
		corinthians.setEscudo("../../assets/icon/corinthians.svg");
		corinthians.setEstado("SP");
		corinthians.setEstadio("Itaquerão");
		corinthians.setSigla("COR");
		corinthians.setElenco(elencoCOR);

		TimeEntity cruzeiro = new TimeEntity();
		cruzeiro.setNome("Cruzeiro");
		cruzeiro.setEscudo("../../assets/icon/cruzeiro.svg");
		cruzeiro.setEstado("MG");
		cruzeiro.setEstadio("Mineirão");
		cruzeiro.setSigla("CRU");
		cruzeiro.setElenco(elencoCRU);

		TimeEntity csa = new TimeEntity();
		csa.setNome("CSA");
		csa.setEscudo("../../assets/icon/csa.svg");
		csa.setEstado("AL");
		csa.setEstadio("Rei Pelé");
		csa.setSigla("CSA");
		csa.setElenco(elencoCSA);

		TimeEntity flamengo = new TimeEntity();
		flamengo.setNome("Flamengo");
		flamengo.setEscudo("../../assets/icon/Flamengo.svg");
		flamengo.setEstado("RJ");
		flamengo.setEstadio("Maracanã");
		flamengo.setSigla("FLA");
		flamengo.setElenco(elencoFLA);

		TimeEntity fluminense = new TimeEntity();
		fluminense.setNome("Fluminense");
		fluminense.setEscudo("../../assets/icon/fluminense.svg");
		fluminense.setEstado("RJ");
		fluminense.setEstadio("Maracanã");
		fluminense.setSigla("FLU");
		fluminense.setElenco(elencoFLU);

		TimeEntity fortaleza = new TimeEntity();
		fortaleza.setNome("Fortaleza");
		fortaleza.setEscudo("../../assets/icon/fortaleza.svg");
		fortaleza.setEstado("CE");
		fortaleza.setEstadio("Castelão");
		fortaleza.setSigla("FOR");
		fortaleza.setElenco(elencoFOR);

		TimeEntity goias = new TimeEntity();
		goias.setNome("Goiás");
		goias.setEscudo("../../assets/icon/goias.svg");
		goias.setEstado("GO");
		goias.setEstadio("Serra Dourada");
		goias.setSigla("GOI");
		goias.setElenco(elencoGOI);

		TimeEntity gremio = new TimeEntity();
		gremio.setNome("Grêmio");
		gremio.setEscudo("../../assets/icon/gremio.svg");
		gremio.setEstado("RS");
		gremio.setEstadio("Arena do Grêmio");
		gremio.setSigla("GRE");
		gremio.setElenco(elencoGRE);

		TimeEntity internacional = new TimeEntity();
		internacional.setNome("Internacional");
		internacional.setEscudo("../../assets/icon/internacional.svg");
		internacional.setEstado("RS");
		internacional.setEstadio("Beira Rio");
		internacional.setSigla("INT");
		internacional.setElenco(elencoINT);

		TimeEntity palmeiras = new TimeEntity();
		palmeiras.setNome("Palmeiras");
		palmeiras.setEscudo("../../assets/icon/palmeiras.svg");
		palmeiras.setEstado("SP");
		palmeiras.setEstadio("Allianz Parque");
		palmeiras.setSigla("PAL");
		palmeiras.setElenco(elencoPAL);

		TimeEntity santos = new TimeEntity();
		santos.setNome("Santos");
		santos.setEscudo("../../assets/icon/santos.svg");
		santos.setEstado("SP");
		santos.setEstadio("Vila Belmiro");
		santos.setSigla("SAN");
		santos.setElenco(elencoSAN);

		TimeEntity saoPaulo = new TimeEntity();
		saoPaulo.setNome("São Paulo");
		saoPaulo.setEscudo("../../assets/icon/sao-paulo.svg");
		saoPaulo.setEstado("SP");
		saoPaulo.setEstadio("Morumbi");
		saoPaulo.setSigla("SAO");
		saoPaulo.setElenco(elencoSAO);

		TimeEntity vasco = new TimeEntity();
		vasco.setNome("Vasco");
		vasco.setEscudo("../../assets/icon/vasco.svg");
		vasco.setEstado("RJ");
		vasco.setEstadio("São Januário");
		vasco.setSigla("VAS");
		vasco.setElenco(elencoVAS);

		timeRepository.save(athleticoPR);
		timeRepository.save(atleticoMG);
		timeRepository.save(avai);
		timeRepository.save(bahia);
		timeRepository.save(botafogo);
		timeRepository.save(ceara);
		timeRepository.save(chapecoense);
		timeRepository.save(corinthians);
		timeRepository.save(cruzeiro);
		timeRepository.save(csa);
		timeRepository.save(flamengo);
		timeRepository.save(fluminense);
		timeRepository.save(fortaleza);
		timeRepository.save(goias);
		timeRepository.save(gremio);
		timeRepository.save(internacional);
		timeRepository.save(palmeiras);
		timeRepository.save(santos);
		timeRepository.save(saoPaulo);
		timeRepository.save(vasco);
	}
	/**
	 * CONSULTAR TODAS AS PESSOAS
	 * 
	 * @return
	 */
	@RequestMapping(value = "/time", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<TimeEntity> consultar() {

		return this.timeRepository.findAll();
	}

	/**
	 * BUSCAR UMA PESSOA PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/time/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody TimeEntity buscar(@PathVariable("codigo") Long codigo) {

		return this.timeRepository.getOne(codigo);
	}
	
	/**
	 * BUSCAR UMA PESSOA PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/time/sigla/{sigla}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody TimeEntity buscar1(@PathVariable("sigla") String sigla) {

		return this.timeRepository.findTimeBySigla(sigla);
	}
	
	 /** BUSCAR ELENCO PELO NOME DO TIME
	 * 
	 * @param sigla
	 * @return
	 */
	@RequestMapping(value = "/time/elenco/{sigla}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<JogadorEntity> buscar(@PathVariable("sigla") String sigla) {
		TimeEntity entidade = this.timeRepository.findTimeBySigla(sigla);
		return entidade.getElenco();
	}

	/***
	 * EXCLUIR UM REGISTRO PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/time/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {

		TimeEntity timeEntity = timeRepository.getOne(codigo);

		try {

			timeRepository.delete(timeEntity);

			return new ResponseModel(1, "Registro excluido com sucesso!");

		} catch (Exception e) {
			return new ResponseModel(0, e.getMessage());
		}
	}

}
