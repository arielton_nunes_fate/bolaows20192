package br.edu.uniateneu.BolaoWS.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.BolaoWS.model.ResponseModel;
import br.edu.uniateneu.BolaoWS.model.UsuarioEntity;
import br.edu.uniateneu.BolaoWS.repository.UsuarioRepository;

//@CrossOrigin(origins = "http://localhost:4200",)
@RestController
@RequestMapping("/service")
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	/**
	 * SALVAR UM NOVO REGISTRO
	 * 
	 * @param usuario
	 * @return
	 */
	@RequestMapping(value = "/usuario", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel salvar(@RequestBody UsuarioEntity usuario) {

		try {

			this.usuarioRepository.save(usuario);

			return new ResponseModel(1, "Registro salvo com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * ATUALIZAR O REGISTRO DE UMA PESSOA
	 * 
	 * @param usuario
	 * @return
	 */
	@RequestMapping(value = "/usuario", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel atualizar(@RequestBody UsuarioEntity usuario) {

		try {

			this.usuarioRepository.save(usuario);

			return new ResponseModel(1, "Registro atualizado com sucesso!");

		} catch (Exception e) {

			return new ResponseModel(0, e.getMessage());
		}
	}

	/**
	 * CONSULTAR TODAS AS PESSOAS
	 * 
	 * @return
	 */
	@RequestMapping(value = "/usuario", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<UsuarioEntity> consultar() {

		return this.usuarioRepository.findAll();
	}

	/**
	 * BUSCAR UMA PESSOA PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/usuario/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody UsuarioEntity buscar(@PathVariable("codigo") Long codigo) {

		return this.usuarioRepository.getOne(codigo);
	}

	/**
	 * BUSCAR UM USUáRIO PELO NOME
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/usuario/login/{login}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody UsuarioEntity buscar(@PathVariable("login") String login) {

		return this.usuarioRepository.findUsuarioByLogin(login);
	}

	/***
	 * EXCLUIR UM REGISTRO PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */

	/**
	 * BUSCAR UM USUáRIO PELO NOME
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/usuario/logar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel buscar(@RequestParam("login") String login,
			@RequestParam("senha") String senha) {
		UsuarioEntity usuario = this.usuarioRepository.findUsuarioByLoginAndSenha(login, senha);
		return usuario != null ? new ResponseModel(200, "Usuario logado com sucesso")
				: new ResponseModel(404, "Usuario não encontrado");
	}
	
	@RequestMapping(value = "/usuario/registrar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Boolean busca(@RequestParam("login") String login,@RequestParam("senha") String senha) {
		UsuarioEntity usuario = this.usuarioRepository.findUsuarioByLoginAndSenha(login, senha); 
		return usuario!=null?true:false;
	}
	
	/***
	 * EXCLUIR UM REGISTRO PELO CÓDIGO
	 * 
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value = "/usuario/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody UsuarioEntity login(@RequestBody UsuarioEntity usuario) {

		UsuarioEntity usuarioResposta = null;
		try {

			usuarioResposta = this.usuarioRepository.findUsuarioByLoginAndSenha(usuario.getLogin(), usuario.getSenha());

		} catch (Exception e) {

		}

		return usuarioResposta;
	}

	@RequestMapping(value = "/usuario/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {

		UsuarioEntity usuarioEntity = usuarioRepository.getOne(codigo);

		try {

			usuarioRepository.delete(usuarioEntity);

			return new ResponseModel(1, "Registro excluido com sucesso!");

		} catch (Exception e) {
			return new ResponseModel(0, e.getMessage());
		}
	}

}
