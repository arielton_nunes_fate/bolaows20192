package br.edu.uniateneu.BolaoWS.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="tb_time")
public class TimeEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="cd_time")
	private Long id;
	@Column(name="nm_time")
	private String nome;
	@Column(name="nm_estado")
	private String estado;
	@Column(name="nm_escudo")
	private String escudo;
	@OneToMany(mappedBy = "time",fetch = FetchType.LAZY)
	@JsonManagedReference
	private List<JogadorEntity> elenco;
	@Column(name="nm_estadio")
	private String estadio;
	@Column(name="nm_sigla")
	private String sigla;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEscudo() {
		return escudo;
	}

	public void setEscudo(String escudo) {
		this.escudo = escudo;
	}

	public List<JogadorEntity> getElenco() {
		return elenco;
	}

	public void setElenco(List<JogadorEntity> elenco) {
		this.elenco = elenco;
	}

	public String getEstadio() {
		return estadio;
	}

	public void setEstadio(String estadio) {
		this.estadio = estadio;
	}

}
