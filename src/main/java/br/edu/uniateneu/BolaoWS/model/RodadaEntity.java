package br.edu.uniateneu.BolaoWS.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tb_rodada")
public class RodadaEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="cd_rodada")
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="nm_rodada")
	private Integer numero;

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@OneToMany(mappedBy = "rodada",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PartidaEntity> partidas;

	public List<PartidaEntity> getPartidas() {
		return partidas;
	}

	public void setPartidas(List<PartidaEntity> partidas) {
		this.partidas = partidas;
	}
}
